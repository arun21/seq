﻿using System.IO;
using Seq.Apps;
using Seq.Apps.LogEvents;
using System;

namespace Seq.Apps.Custom.FirstOfType
{
    [SeqApp("Custom First Of Type", Description = "Emits an event whenever the first event of a new type is seen. Currently only suitable when the event stream is Serilog-based.")]
    public class FirstOfTypeDetector : Reactor, ISubscribeTo<LogEventData>
    {
        const string StateFilename = "DetectedEventTypes.bin";
        const string StateWriteFilename = "DetectedEventTypes-new.bin";
        const string StateBackupFilename = "DetectedEventTypes-old.bin";

        // Storing path wihich provides a unique filesystem folder for the running app instance
        string stateFile = string.Empty;
        UInt32BloomFilter _filter;
        // Time to reset the First Of Type
        TimeSpan _suppressionTime;
        // Tracks when reset is due until
        DateTime? _suppressUntilUtc;
    }
}

